# Applied Chemical Process Engineering
## Numerical fit of the reaction kinetics to experimental values

Authors: Matthias Geiger, Daniel Felder
AVT.CVT RWTH Aachen Universtiy

This is a collection of documents and student handouts for the python-part of applied chemical process engineering. 

Questions, remarks and suggestions for improvements are welcome at: matthias.geiger@avt.rwth-aachen.de

### Contents: 
+ ACVT_Students_KineticFit.ipynb
	* Student handout providing detailed instructions and workspace
+ ACVT_MasterSolution.ipynb
	* Reference solution for the instructors
	* Should not be seen as the only solution, all students will find slightly different ways of solving the problem
+ Intro to Jupyter and Python.ipynb
	* Short introduction into juypter and python
	* Prior experience with programming/coding is required
+ Explicit-Euler.png
	* Image to illustrate the explicit euler
	* Should be placed into the same folder as the jupyter notebook to ensure correct representation
+ SI_SI_Hands-on Kinetic Measurements and Simulation.pdf
	* Full supporting information of the publication "Hands-on Kinetic Measurements and Simulation for Chemical Process Engineering Students"